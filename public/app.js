var app = new Vue({
    el: '#app',
    data: {
      initialDateTimestamp: new Date("10/7/2019"),
      tasks: [ 'Kitchen', 'Shower', 'Bin/Trash', 'Corridor/Floor', 'Toilet' ],
      tasksImg: [ './img/kitchen.jpg', './img/shower.jpg', './img/bin_trash.jpg', './img/floor.jpg', './img/toilet.jpg' ], 
      people: null,
      actualDateTimestamp: null
    },
    created() {
        const week = 1000*7*24*60*60;
        let rooms = ['Zimmer 5', 'Zimmer 2', 'Zimmer 4', 'Zimmer 1', 'Zimmer 3'];
        this.actualDateTimestamp = Date.now();
        let nbrOfWeeksSinceInitial = Math.floor(( this.actualDateTimestamp - this.initialDateTimestamp) / week);
        let nbrShifts = this.computeNbrOfShits( nbrOfWeeksSinceInitial );
        this.people = this.shitArray(rooms, 1, nbrShifts);
        //console.log(this.people);
    },
    methods: {
        shitArray(arr, direction, n) {
            var times = n > arr.length ? n % arr.length : n;
            return arr.concat(arr.splice(0, (direction > 0 ? arr.length - times : times)));
        },
        computeNbrOfShits( nbr ){
            let tmp = true;
            let res = ((nbr % 5) === 0) ? (nbr/5) : (nbr%5);
            while(tmp) {
                let temp = ((res % 5) === 0) ? (res/5) : (res%5);
                tmp = temp > 5;
            }
            return res;
        }
    },
    computed: {
        getWellFormatedDate() {
            let date = new Date(this.actualDateTimestamp);
            return `${date.getUTCDate()} / ${date.getUTCMonth() +1} / ${date.getUTCFullYear()}`;
        }
    }

  })